import org.apache.commons.cli.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Bts {
    public static void main(String[] args) {

        String progName = "bts";
        String version = "0.0.1";
        Options options = new Options();
        HelpFormatter formatter = new HelpFormatter();

        options.addOption("h", "Show help options");
        options.addOption("v", "Display version number");
        options.addOption("V", "Verbose mode. Default: off");

        CommandLineParser parser = new DefaultParser();

        try {
            CommandLine cmd = parser.parse(options, args);
            if(cmd.hasOption("h")) {
                formatter.printHelp(progName + " [OPTIONS]", options);
                System.exit(0);
            } else if (cmd.hasOption("v")) {
                System.out.println(progName + " v" + version);
                System.exit(0);
            } else {
                String fileName = "/sys/class/power_supply/BAT0/uevent";
                String line = null;

                try {
                    FileReader fileReader = new FileReader(fileName);
                    if(cmd.hasOption("V")) {
                        System.out.print("\nSuccessful opened: " + fileName + "\n\n");
                    }
                    BufferedReader bufferedReader = new BufferedReader(fileReader);

                    if (cmd.hasOption("V")) {
                        System.out.format("%-35s %s\n", "Name","Value" );
                        System.out.format("%-35s %s\n", "----", "-----" );
                    }
                    while((line = bufferedReader.readLine()) != null) {
                        String header = line.split("=")[0];
                        String value = line.split("=")[1].trim();
                        System.out.format("%-35s %s\n", header, value);
                    }
                    System.out.println();
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    System.exit(1);
                }
            }
        } catch (ParseException e){
            System.out.println(progName + ": " + e.getMessage());
            System.out.println("Try \'" + progName + " -h\' for more information");
            System.exit(1);
        }
    }
}